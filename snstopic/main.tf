
resource "aws_sns_topic" "conilius_topic" {
  name     = "conilius_topic"
}

resource "aws_sns_topic_policy" "conilius_policy" {
  depends_on = ["aws_sns_topic.conilius_topic"]
  arn = "${aws_sns_topic.conilius_topic.arn}"
  policy = <<POLICY
{
  "Version": "2008-10-17",
  "Id": "__default_policy_ID",
  "Statement": [
    {
      "Sid": "__default_statement_ID",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish",
        "SNS:Receive"
      ],
      "Resource": "${aws_sns_topic.conilius_topic.arn}",
      "Condition": {
        "StringEquals": {
          "AWS:SourceOwner": "000000000000"
        }
      }
    }
  ]
}
POLICY
}

resource "aws_sns_topic_subscription" "conilius_topic_subscription" {
  count = "2"
  depends_on = ["aws_sns_topic.conilius_topic"]
  topic_arn = "${aws_sns_topic.conilius_topic.arn}"
  protocol  = "email"
  endpoint  = "aminkengcf78@yahoo.com"
}