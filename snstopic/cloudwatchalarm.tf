
resource "aws_cloudwatch_composite_alarm" "coniliuswatch_alarm" {
  alarm_description = "This is a composite alarm!"
  alarm_name        = "coniliuswatch_alarm"

  alarm_actions = [aws_sns_topic.conilius_topic.arn]
  ok_actions    = [aws_sns_topic.conilius_topic.arn]

  alarm_rule = "ALARM(${aws_cloudwatch_metric_alarm.node_count.alarm_name}) OR ALARM(${aws_cloudwatch_metric_alarm.node_count.alarm_name})"

  depends_on = [
      aws_cloudwatch_metric_alarm.node_count,
      aws_sns_topic.conilius_topic,
      aws_sns_topic_subscription.conilius_topic_subscription
  ]
}

resource "aws_cloudwatch_dashboard" "main" {
  dashboard_name = "conilius-dashboard"

  dashboard_body = <<EOF
{
  "widgets": [
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 6,
      "properties": {
        "metrics": [
          [
            "AWS/EC2",
            "CPUUtilization",
            "InstanceId",
            "i-012345"
          ]
        ],
        "period": 300,
        "stat": "Average",
        "region": "us-east-1",
        "title": "EC2 Instance CPU"
      }
    },
    {
      "type": "text",
      "x": 0,
      "y": 7,
      "width": 3,
      "height": 3,
      "properties": {
        "markdown": "Hello world"
      }
    }
  ]
}
EOF
}

resource "aws_cloudwatch_metric_alarm" "node_count" {
  alarm_name                = "cloudwatch_alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = "60"
  statistic                 = "Average"
  threshold                 = "70"
  alarm_description         = "This metric monitors ec2 cpu utilization"
  insufficient_data_actions = []
}

resource "aws_cloudwatch_log_group" "ebs_log_group" {
  name = "ebs_log_group"
  retention_in_days = 30
}